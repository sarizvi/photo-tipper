# Photo Tipper

A full stack dapp where user can upload images which are managed and stored through IPFS and users can tip the images using Ether.

## Dependencies

```bash
Node.js
Ganache (one click blockchain)
Truffle framework (for writing smart contracts and tests) Command: npm install --g truffle@5.1.14
Metamask (To connect your browser to blockchain for the dapp to function)
Install dependencies using: npm install
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

